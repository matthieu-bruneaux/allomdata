[![GitLab pipeline status](https://gitlab.com/matthieu-bruneaux/allomdata/badges/master/pipeline.svg)](https://gitlab.com/matthieu-bruneaux/allomdata/commits/master)
[![R CMD check](https://matthieu-bruneaux.gitlab.io/allomdata/R-CMD-check_badge.svg)](https://matthieu-bruneaux.gitlab.io/allomdata/R-CMD-check_output.txt)
[![Lifecycle Status](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/)

# allomdata: an R package of allometric datasets

## Quick install

Installing from the GitLab repository should be as easy as typing:

```
# Run inside an R session
install.packages("devtools")
devtools::install_gitlab("matthieu-bruneaux/allomdata")
```
Please let us know if you encounter any issues during the package installation!

## Documentation

The package documentation is [available here](https://matthieu-bruneaux.gitlab.io/allomdata/) (built with `pkgdown`).

## TODO

Data to add?

- "Mammalian metabolic allometry: do intraspecific variation, phylogeny, and regression models matter?" https://datadryad.org/stash/dataset/doi:10.5061/dryad.712
- "The role of gravity in the evolution of mammalian blood pressure" https://datadryad.org/stash/dataset/doi:10.5061/dryad.dv1j5
- "The evolution of energetic scaling across the vertebrate tree of life" https://datadryad.org/stash/dataset/doi:10.5061/dryad.3c6d2
- "Allometric scaling of metabolism, growth, and activity in whole colonies of the seed harvester ant, Pogonomyrmex californicus" https://datadryad.org/stash/dataset/doi:10.5061/dryad.1594
- "Developmental plasticity in metabolism but not in energy reserve accumulation in a seasonally polyphenic butterfly" https://datadryad.org/stash/dataset/doi:10.5061/dryad.1f8m3q1 (respiration data with a few, rare negative values)


## Contact

- [Matthieu Bruneaux](mailto:matthieu.bruneaux@gmail.com)
